CV_FILENAME_STEM:=CV-ToonClaes-

auto_lang=$(patsubst $(CV_FILENAME_STEM)%,%,$*)

all: nl en

nl: public/CV-ToonClaes-nl.pdf
en: public/CV-ToonClaes-en.pdf

public/%.pdf: public/ %.tex
	$(PDFLATEX) $<

public/%.pdf: public/ CV-ToonClaes-multi.tex
	sed -E "s/\\setdoclang\{[^\}]+\}\{[^\}]+\}/\\setdoclang\{$(auto_lang)\}\{$(auto_lang)\}/" CV-ToonClaes-multi.tex | $(PDFLATEX) --jobname=$*
	mv $*.pdf $@

public/:
	mkdir $@

public/assets/toonclaes-pubkey.asc:
	gpg --export --armor toon@iotcl.com > $@

clean:
	rm -f *.aux *.log *.out

distclean: clean
	rm -f public/*.pdf

.PHONY: all nl clean distclean

OS := $(shell uname)
ifeq ($(OS),Darwin)
  #TEXDIR:=/usr/texbin/
  TEXDIR?=/Library/TeX/texbin
  PDFLATEX:=$(TEXDIR)/pdflatex
else
  PDFLATEX:=pdflatex
endif
